FROM node:14-alpine

COPY . /app

WORKDIR app

RUN apk add --update make

RUN npm install

RUN npm run build

RUN rm -rf src

ENTRYPOINT ["make"]
CMD ["quick"]
