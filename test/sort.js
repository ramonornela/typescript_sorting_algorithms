class SortCount 
{
  constructor(sortClass)
  {
    this.countCompared = 0;
    this.countVisited = 0;
    this.sort = this.createSort(sortClass);
  }

  reset() {
    this.countCompared = 0;
    this.countVisited = 0;
  }

  createSort(sortClass) {
    return new sortClass({
      visitingCallback: this.visitingCallback.bind(this),
      compareCallback: this.compareCallback.bind(this)
    });
  }

  visitingCallback() {
    this.countVisited++;
  }
  
  compareCallback(a, b) {
    this.countCompared++;
  
    if (a === b) {
      return 0;
    }
  
    return a < b ? -1 : 1;
  }
}

module.exports = (sortClass) => {
  return new SortCount(sortClass);
};
