const BubbleSort = require('../lib/bubblesort').default;
const sortCount = require('./sort');
const logs = require('./logs');

logs(sortCount(BubbleSort), 'Bubble');
