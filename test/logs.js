const data = require('./data');

const runWithTimer = (sortCount, type, quantity, options) => {
  const { sort } = sortCount;

  const { label, debug } = options;

  const d = data(type, quantity);
  const startTime = Date.now();
  const sortArray = sort.sort(d);

  const logs = {
    time: ((Date.now() - startTime) / 1000).toFixed(6),
    compare: sortCount.countCompared,
    visited: sortCount.countVisited
  };

  if (debug) {
    console.log('Order: ', sortArray);
    console.log(`${label} ${quantity}/${type} result: `, logs);
  }

  sortCount.reset();
  return { sortArray, ...logs };
};

module.exports = (sortCount, label, debug = true) => {
  const scenarios = {
    asc: [1000, 10000, 100000],
    desc: [1000, 10000, 100000],
    random: [1000, 10000, 100000],
  };

  const result = {};
  for (const typeOrder of Object.keys(scenarios)) {
    const options = { label, debug };
    result[typeOrder] = scenarios[typeOrder].map(quantity => runWithTimer(sortCount, typeOrder, quantity, options));
  }

  return result;
};
