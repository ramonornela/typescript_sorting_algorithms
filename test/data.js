// @see https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
const shuffleArray = array => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

const buildSequence = limit => {
  return [...Array(limit).keys()];
};

module.exports = (type, quantity) => {  
  let numbers = buildSequence(quantity);

  if (type === 'desc') {
    numbers = numbers.reverse();
  } else if (type === 'random') {
    shuffleArray(numbers);
  }

  return numbers;
};
