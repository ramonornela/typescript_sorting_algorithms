const QuickSort = require('../lib/quicksort').default;
const sortCount = require('./sort');
const logs = require('./logs');

logs(sortCount(QuickSort), 'Quick');
