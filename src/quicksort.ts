import Sort from './sort';

function partitionHoare(array, left, right) {
  const pivot = Math.floor((left + right) / 2);

  this.callbacks.visitingCallback(pivot);

  const comparator = this.comparator;

  while (comparator.lessThanOrEqual(left, right)) {
    while (comparator.lessThan(array[left], array[pivot])) {
      left++;
    }

    while (comparator.greaterThan(array[right], array[pivot])) {
      right--;
    }

    if (comparator.lessThanOrEqual(left, right)) {
      [array[left], array[right]] = [array[right], array[left]];
      left++;
      right--;
    }
  }

  return left;
}
export default class QuickSort extends Sort
{
  sort(
    array: any[],
    inputLowIndex = 0,
    inputHighIndex = array.length - 1
  ) {
    const pivot = partitionHoare.call(this, array, inputLowIndex, inputHighIndex);

    if (inputLowIndex < pivot - 1) {
      this.sort(array, inputLowIndex, pivot - 1);
    }

    if (inputHighIndex > pivot) {
      this.sort(array, pivot, inputHighIndex);
    }

    return array;
  }
}
