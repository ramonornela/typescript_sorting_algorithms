export type FnComparator = (a: string | number, b: string | number) => 0 | -1 | 1
