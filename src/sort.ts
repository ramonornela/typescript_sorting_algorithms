import { FnComparator } from './constants';
import Comparator from './comparator';

type CallbacksType = { compareCallback: FnComparator, visitingCallback: Function };

const initSortingCallbacks = (originalCallbacks: any): CallbacksType => {
  const callbacks = originalCallbacks || {};
  const stubCallback = () => {};

  callbacks.compareCallback = callbacks.compareCallback || undefined;
  callbacks.visitingCallback = callbacks.visitingCallback || stubCallback;

  return callbacks;
};

export default abstract class Sort
{
  protected readonly callbacks: CallbacksType;

  protected readonly comparator: Comparator;

  constructor(originalCallbacks: CallbacksType) {
    this.callbacks = initSortingCallbacks(originalCallbacks);
    this.comparator = new Comparator(this.callbacks.compareCallback);
  }

  abstract sort(originalArray: any[], ...args): any[];
}
