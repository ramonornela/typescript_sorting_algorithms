import { FnComparator } from './constants';

const compareFunctionDefault = (a: string | number, b: string | number): 0 | -1 | 1 => {
  if (a === b) {
    return 0;
  }

  return a < b ? -1 : 1;
}

export default class Comparator
{
  compare: FnComparator;
  /**
   * @param {function(a: *, b: *)} [compareFunction] - It may be custom compare function that, let's
   * say may compare custom objects together.
   */
  constructor(compareFunction: FnComparator)
  {
    this.compare = compareFunction || compareFunctionDefault;
  }

  equal(a: string | number, b: string | number): boolean
  {
    return this.compare(a, b) === 0;
  }

  lessThan(a: string | number, b: string | number): boolean
  {
    return this.compare(a, b) < 0;
  }

  greaterThan(a: string | number, b: string | number): boolean
  {
    return this.compare(a, b) > 0;
  }

  lessThanOrEqual(a: string | number, b: string | number): boolean
  {
    return this.lessThan(a, b) || this.equal(a, b);
  }

  greaterThanOrEqual(a: string | number, b: string | number): boolean
  {
    return this.greaterThan(a, b) || this.equal(a, b);
  }

  /**
   * Reverses the comparison order.
   */
  reverse(): void
  {
    const compareOriginal = this.compare;
    this.compare = (a: string | number, b: string | number) => compareOriginal(b, a);
  }
}
